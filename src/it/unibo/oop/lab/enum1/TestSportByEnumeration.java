package it.unibo.oop.lab.enum1;

import java.util.ArrayList;
import java.util.List;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        final SportSocialNetworkUserImpl<User> vRossi = new SportSocialNetworkUserImpl<>("Valentino", "Rossi", "theDoctor", 36);
        final SportSocialNetworkUserImpl<User> fTotti = new SportSocialNetworkUserImpl<>("Francesco", "Totti", "erPupone", 40);
        final SportSocialNetworkUserImpl<User> jLebron = new 
        		SportSocialNetworkUserImpl<>("James", "Lebron", "jlb", 30);
        final SportSocialNetworkUserImpl<User> cRonaldo = new SportSocialNetworkUserImpl<>("Cristiano", "Ronaldo", "cr7", 28);
        final SportSocialNetworkUserImpl<User> fAlonso = new SportSocialNetworkUserImpl<>("Fernando", "Alonso", "fAlonso", 29);
        
        vRossi.addSport(Sport.MOTOGP);
        fTotti.addSport(Sport.SOCCER);
        jLebron.addSport(Sport.BASKET);
        cRonaldo.addSport(Sport.SOCCER);
        fAlonso.addSport(Sport.F1);
        
        fTotti.addFollowedUser("colleagues", cRonaldo);
        fTotti.addFollowedUser("motors", fAlonso);
        fTotti.addFollowedUser("motors", vRossi);
        fTotti.addFollowedUser("myths", jLebron);
        
        final List<User> tottiFrMotors = new ArrayList<>();
        tottiFrMotors.addAll(fTotti.getFollowedUsersInGroup("motors"));
        System.out.println("Totti has 2 friends who ride: " + (tottiFrMotors.size() == 2));
        
        final List<User> tottiColleagues = new ArrayList<>();
        tottiColleagues.addAll(fTotti.getFollowedUsersInGroup("colleagues"));
        System.out.println("Totti has only one colleague: " + (tottiColleagues.size() == 1));
        
        final List<User> tottiVolley = new ArrayList<>();
        tottiVolley.addAll(fTotti.getFollowedUsersInGroup("volley"));
        System.out.println("Totti has no followers who play volleyball: " + tottiVolley.isEmpty());
        
        System.out.println("Totti is a soccer: " + fTotti.hasSport(Sport.SOCCER));
    }

}
