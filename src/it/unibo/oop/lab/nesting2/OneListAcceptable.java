package it.unibo.oop.lab.nesting2;

import java.util.Iterator;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {
	
	private List<T> list;
	
	public OneListAcceptable(final List<T> list){
		this.list = list;
	}

	@Override
	public Acceptor<T> acceptor() {
		// TODO Auto-generated method stub
		Iterator<T> iterator = list.iterator();
		return new Acceptor<T>() {

			@Override
			public void accept(T newElement) throws ElementNotAcceptedException {
				// TODO Auto-generated method stub
				 try {
	                    if (newElement.equals(iterator.next())) {
	                        return;
	                    }
	                } catch (Exception e) {
	                    System.out.println("no more element to be evaluated");
	                }
	                throw new Acceptor.ElementNotAcceptedException(newElement);
			}

			@Override
			public void end() throws EndNotAcceptedException {
				// TODO Auto-generated method stub
				 try {
	                    if (iterator.next() == null) {
	                        return;
	                    }
	                } catch (Exception e) {
	                    System.out.println("no more element to be evaluated");
	                }
	                throw new Acceptor.EndNotAcceptedException();
			}
			
		};
	}

}
